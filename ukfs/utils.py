#############################################################################################################
# Author :
#   Jerome Mariette, MIAT, Universite de Toulouse, INRA 31326 Castanet-Tolosan France
#   Nathalie Villa-Vialaneix, MIAT, Universite de Toulouse, INRA 31326 Castanet-Tolosan France
#
# Copyright (C) 2017
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
#############################################################################################################

import numpy as np
import autograd.numpy as np

def euclidean_dist(x1, x2):
    x1p2 = np.sum(np.square(x1), 1)
    x2p2 = np.sum(np.square(x2), 1)
    return x1p2.reshape((-1, 1)) + x2p2.reshape((1, -1)) - 2 * np.dot(x1, x2.T)

def kernel_dist(K):
    return np.asarray([[K[i,i]+K[j,j]-2*K[i,j] for j in range(0, K.shape[0])] for i in range(0, K.shape[0])])

def kernel_loss(w,x,K,kernel_sel,**kwargs):
    return np.sum(np.square(K-kernel_sel(w,x,x,**kwargs)))
    
def kernel_loss_log(w,x,K,kernel_sel,**kwargs):
    return np.log(np.sum(np.square(K-kernel_sel(w,x,x,**kwargs))) + 1)

def kpca_loss(w,x,X_kpca,kernel_sel,**kwargs):
    Kpca_dist = euclidean_dist(X_kpca,X_kpca)
    Kpca_dist[Kpca_dist < 0] = 0
    return np.sum( np.square( kernel_dist(kernel_sel(w,x,x,**kwargs)) - Kpca_dist ) )

def graph_loss(w,x,K,Lg,mu,kernel_sel,**kwargs):
    return np.sum(np.square(K-kernel_sel(w,x,x,**kwargs))) + mu * np.dot(np.dot(w.T,Lg),w)

def kpca_func(K, n_components=None):
    
    # first center the kernel
    n_samples = K.shape[0]
    K_fit_rows = np.sum(K, axis=0) / n_samples
    K = K - K_fit_rows - (np.sum(K, axis=1) / K_fit_rows.shape[0])[:, np.newaxis] + K_fit_rows.sum() / n_samples
    
    if n_components is None:
        n_components = K.shape[1]
    else:
        n_components = min(K.shape[1], int(n_components))
    lambdas, alphas = eigh(K, eigvals=(K.shape[1] - n_components, K.shape[1] - 1))

    # sort eigenvectors in descending order
    indices = lambdas.argsort()[::-1]
    lambdas = lambdas[indices]
    alphas = alphas[:, indices]
    
    # remove eigenvectors with a zero eigenvalue
    tolerance = 10**(-10)
    lambdas = np.round_(lambdas, int(-np.log10(tolerance)))
    
    alphas = alphas / np.sqrt(lambdas)
    X_transformed = alphas * lambdas
    print(lambdas)
    print(alphas)
    print(X_transformed)
    return (lambdas, alphas, X_transformed)
####

#### linear kernel functions
def linear(x1, x2):
    return np.dot(x1,np.transpose(x2))

def linear_sel(w,x1,x2):
    d=w.shape[0]
    return linear(np.reshape(w,(1,d))*x1,np.reshape(w,(1,d))*x2)
####

#### gaussian.radial.basis kernel functions
def gaussian_radial_basis(x1, x2, sigma=1):
    return np.exp(-sigma*euclidean_dist(x1,x2))

def gaussian_radial_basis_sel(w, x1, x2, sigma=1):
    d=w.shape[0]
    return np.exp(-sigma*euclidean_dist(np.reshape(w,(1,d))*x1,np.reshape(w,(1,d))*x2))
####

#### bray-curtis dissimilarity functions
def bray(x1,x2):
    eps=1
    return np.sum(np.abs(x1[:,None,:]-x2[None,:,:]),2)/(np.sum((x1[:,None,:]+x2[None,:,:]),2)+eps) 

def bray_sel(w,x1,x2):
    d=w.shape[0]
    return bray(np.reshape(w,(1,d))*x1,np.reshape(w,(1,d))*x2)
####

#### bray-curtis kernel functions (for kpca)
def brayK(x1,x2):
    eps=0
    diss=np.sum(np.abs(x1[:,None,:]-x2[None,:,:]),2)/(np.sum((x1[:,None,:]+x2[None,:,:]),2)+eps)
    return -0.5 * np.dot(np.dot(np.identity(diss.shape[0]) - 1 / diss.shape[0], diss), np.identity(diss.shape[0]) - 1 / diss.shape[0]) 
    
def brayK_sel(w,x1,x2):
    d=w.shape[0]
    return brayK(np.reshape(w,(1,d))*x1,np.reshape(w,(1,d))*x2)
