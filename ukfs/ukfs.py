#############################################################################################################
# Author :
#   Jerome Mariette, MIAT, Universite de Toulouse, INRA 31326 Castanet-Tolosan France
#   Nathalie Villa-Vialaneix, MIAT, Universite de Toulouse, INRA 31326 Castanet-Tolosan France
#
# Copyright (C) 2017
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
#############################################################################################################

import autograd
import numpy as np
from scipy.spatial.distance import pdist
from scipy.optimize import minimize
from scipy.linalg import eigh
import autograd.numpy as np
from .solvers import fmin_prox, reg_l1, prox_l1
from .utils import *

which = lambda lst:list(np.where(lst)[0])

def ukfspy(x, kernel_func="linear", method="kernel", keepX=None, reg=1, n_components=2, Lg=None, mu=1, max_iter=100, kwargs={}):
    
    # force x to be a numpy array
    x = np.array(x)
    
    # optim parameters
    params=dict()
    params['nbitermax']=max_iter
    params['stopvarx']=1e-9
    params['stopvarj']=1e-9
    params['t0']=10.
    params['m_back']=1
    params['verbose']=False
    params['bbrule']=True
    params['log']=False
    
    if kernel_func == "linear":
        kernel = linear
        kernel_sel = linear_sel
        kwargs = {}
    elif kernel_func == "gaussian.radial.basis":
        kernel = gaussian_radial_basis
        kernel_sel = gaussian_radial_basis_sel
        # if no value provided for sigma parameter, find the sigma
        # that maximizes projections distances in the KPCA space
        if "sigma" not in kwargs:
            def opt_rbf_sigma (sigma):
                K = gaussian_radial_basis(x,x,sigma[0])
                (lambdas, alphas, X_kpca) = kpca_func(K, 2)
                all_dist = pdist(X_kpca[:,0:2], "euclidean")
                #all_dist = pdist(X_kpca[:,0:2], "euclidean")
                #return -np.sum(all_dist)
                return -np.sum(lambdas)
            kwargs = {"sigma" : minimize(opt_rbf_sigma, [0.1], method="L-BFGS-B", bounds=[(1e-12, 1)]).x[0]}
    elif kernel_func == "bray":
        if method == "kpca":
            # if kpca use the kernel version
            kernel = brayK
            kernel_sel = brayK_sel
        else:
            # if kernel and graph use the dissimilarity
            kernel = bray
            kernel_sel = bray_sel

        # only if differentiable
        params['bbrule']=False
        kwargs = {}
    
    K = kernel(x,x,**kwargs)
    
    if method == "kernel":
        grad_loss_K = autograd.grad(kernel_loss,0)
        # parameters for optim (loss/reg)
        f=lambda w:kernel_loss(w,x,K,kernel_sel,**kwargs) #  loss
        df=lambda w:grad_loss_K(w,x,K,kernel_sel,**kwargs) # grad  loss
    elif method == "kpca":
        (lambdas, alphas, X_kpca) = kpca_func(K, n_components)
        grad_loss_K = autograd.grad(kpca_loss,0)
        # parameters for optim (loss/reg)
        f=lambda w:kpca_loss(w,x,X_kpca,kernel_sel,**kwargs) #  loss
        df=lambda w:grad_loss_K(w,x,X_kpca,kernel_sel,**kwargs) # grad  loss
    elif method == "graph":
        grad_loss_K = autograd.grad(graph_loss,0)
        Lg = np.array(Lg)
        # parameters for optim (loss/reg)
        f=lambda w:graph_loss(w,x,K,Lg,mu,kernel_sel,**kwargs) #  loss
        df=lambda w:grad_loss_K(w,x,K,Lg,mu,kernel_sel,**kwargs) # grad  loss
    
    g=reg_l1
    prox_g=lambda x,lamb, **kw  : prox_l1(np.maximum(0,x),lamb,**kw)
    
    w0=np.ones(x.shape[1])
    
    if keepX == None:
        w, log = fmin_prox(f, df, g, prox_g, w0, lambd=reg, **params)
    else:
        # warm restart until number of variables ok
        for i, reg in enumerate([0.001, 0.002, 0.003, 0.004, 0.005, 0.006, 0.007, 0.008, 0.009, 0.01, 0.02, 0.03, 0.04, 0.05, 0.06, 0.07, 0.08, 0.09, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10]):
            neww, log = fmin_prox(f, df, g, prox_g, w0, lambd=reg, **params)
            if (len(which(neww>0)) <= keepX):
                w = w0
                break
            else:
                w = neww
            w0 = neww
    
    return (w)
