
""" Generic optimization solvers """

# Author: Remi Flamary <remi.flamary@unice.fr>
#
# License: MIT License

import numpy as np
import autograd.numpy as np

#### optim functions
def reg_l1(x, lambd=1, **kwargs):
    """
    l1 regularization (LASSO)
    x : vector to project
    lambd: regularization term
    """
    return lambd * np.sum(np.abs(x))


def prox_l1(x, lambd=1, **kwargs):
    """
    l1 regularization proximal operator (LASSO)
    x : vector to project
    lambd: regularization term
    """
    return np.sign(x) * np.maximum(np.abs(x) - lambd, 0)

def norm(x):
    """l2 norm of vector (Frobenius for matrices)"""
    return np.sqrt(np.sum(np.square(x)))

# format string for printing in verbose mode
prnt_str_name = "|{it:>5}|{loss:>13}|{dloss:>13}|{step:>13}|\n" \
                "|-----|-------------|-------------|-------------|"
prnt_str_loop = "|{it:5d}|{loss: 10e}|{dloss: 10e}|{step: 10e}|"

def fmin_prox(f, df, g, prox_g, x0, lambd=1., backtrack=True, nbitermax=1000,
              stopvarx=1e-9, stopvarj=1e-9, t0=10., verbose=False, m_back=1,
              sigma=1e-9, eta=2, nbitermax_back=100, bbrule=True, log=False,
              **kwargs):
    r""" Minimize a sum of smooth and nonsmooth function using proximal splitting
    The algorithm is the classical Forward Backward Splitting [1]_
    with BB rule for step estimation [2]_.
    Solve the optimization problem:
    .. math::
        min_x \quad  f(x)+\lambda g(x)
    where:
    - f is differentiable (df) and Lipshictz gradient
    - g is non-differentiable but has a proximal operator (prox_g)
    prox_g is a function providing the solution to problem
    .. math::
        min_x \quad  \frac{1}{2}\|x_0-x\|^2+\lambda*g(x)
    Several proximal operators are available at optim.prox
    Parameters
    ----------
    f : function
        Smooth function f: R^d -> R
    df : function
        Gradient of f, df:R^d -> R^d
    g : function
        Nonsmooth function g: R^d -> R
    prox_g : function
        Proximal of g, df:R^d -> R^d
    x_0 : (d,) numpy.array
        Initial point
    lambda : float
        Regularization parameter >0
    backtrack : boolean, optional
        Perform backtracking if true (default: True).
    bbrule : boolean, optional
        update step with bb rule.
    nbitermax : int, optional
        Max number of iterations in algorithm
    stopvarx : float, optional
        Stopping criterion for relative variation of the
        norm of x
    stopvarj : float, optional
        Stopping criterion for relative variation of the cost
    t0 : float, optional
        initial descent step
    verbose : boolean, optional
        prinrt optimization information
    m_back : int, optional
        Window size for backtrack (if <1 then non decreasing)
    sigma : float, optional
        descent parameter for backtrack
    eta : float, optional
        value multiplying t during backtrack
    nbitermax_back : int, optional
        Max number of backtrack iterations
    Returns
    -------
    x: (d,) ndarray
        Optimal solution x
    val: float
        optimal value of the objective (None if optimization error)
    log: dict
        Optional log output
    References
    ----------
    .. [1] Combettes, P. L., & Wajs, V. R. (2005). Signal recovery by proximal
        forward-backward splitting. Multiscale Modeling & Simulation, 4(4),
        1168-1200.
    .. [2] Barzilai, J., & Borwein, J. M. (1988). Two-point step size
        gradient methods. IMA journal of numerical analysis, 8(1), 141-148.
    See Also
    --------
    optim.prox : Module containing proximal operators
    optim.fmin_proj : Projected gradient (special case of proximal gradient)
    """
    x = x0.copy()
    grad = df(x, **kwargs)
    # grad[:]=0
    loss = list()
    loss.append(f(x, **kwargs) + g(x, lambd, **kwargs))
    if log:
        log = dict()
        log['loss'] = loss
    t = t0
    if verbose:
        print((prnt_str_name.format(it="It. ", loss='Loss ',
                                    dloss="Delta Loss ", step="Step ")))
        print((prnt_str_loop.format(it=0, loss=loss[-1], dloss=0, step=1 / t)))
    loop = True
    it = 1
    while loop:
        x_1 = x.copy()
        grad_1 = grad.copy()
        # gradient
        grad = df(x, **kwargs)
        # prox operator
        x = prox_g(x_1 - grad / t, lambd / t, **kwargs)
        # cost computation
        loss.append(f(x, **kwargs) + g(x, lambd, **kwargs))
        # line search backtrack
        it2 = 0
        thr_back = np.max([loss[-2 - k] - sigma / 2 * t * norm(x - x_1)
                           ** 2 for k in range(min(m_back, len(loss) - 1))])
        while loss[-1] > thr_back and it2 < nbitermax_back and backtrack:
            t = t * eta
            x = prox_g(x_1 - grad / t, lambd / t, **kwargs)
            loss[-1] = f(x, **kwargs) + g(x, lambd, **kwargs)
            thr_back = np.max([loss[-2 - k] - sigma / 2 * t * norm(x - x_1)
                               ** 2 for k in range(min(m_back, len(loss) - 1))
                               ])
            # print '\t',loss[-1],thr_back
            it2 += 1
        if it2 == nbitermax_back:
            print("Warning: backtrack failed")
        # print loss[-1],t
        # print information
        if verbose:
            if not (it) % 20:
                print((prnt_str_name.format(it="It. ", loss='Loss ',
                                            dloss="Delta Loss ",
                                            step="Step ")))
            print(prnt_str_loop.format(it=it,
                                       loss=loss[-1],
                                       dloss=(loss[-1] - loss[-2]
                                              ) / abs(loss[-2]),
                                       step=1 / t))
        # BB rule
        xbb = x - x_1
        ybb = grad - grad_1
        if it >= 1 and norm(xbb) > 1e-12 and norm(ybb) > 1e-12 and bbrule:
            t = abs(np.sum(xbb * ybb) / np.sum(xbb * xbb))
#        else:
#            t=t0
        # test convergence
        if norm(x - x_1) / norm(x) < stopvarx:
            loop = False
            if verbose:
                print("delta x convergence")
        if abs(loss[-1] - loss[-2]) / abs(loss[-2]) < stopvarj:
            loop = False
            if verbose:
                print("delta loss convergence")
        if it >= nbitermax:
            loop = False
            if verbose:
                print("Max number of iteration reached")
        # increment iteration
        it += 1
    if log:
        log['loss'] = loss
        return x, loss[-1], log
    else:
        return x, loss[-1]
