#!/usr/bin/env python

#############################################################################################################
# Author :
#   Jerome Mariette, MIAT, Universite de Toulouse, INRA 31326 Castanet-Tolosan France
#   Nathalie Villa-Vialaneix, MIAT, Universite de Toulouse, INRA 31326 Castanet-Tolosan France
#
# Copyright (C) 2017
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
#############################################################################################################

from setuptools import setup, find_packages
from codecs import open
from os import path
import re
import os

here = path.abspath(path.dirname(__file__))

# dirty but working
__version__ = re.search(
    r'__version__\s*=\s*[\'"]([^\'"]*)[\'"]',  # It excludes inline comment too
    open('ukfs/__init__.py').read()).group(1)

ROOT = os.path.abspath(os.path.dirname(__file__))

# convert markdown readme to rst in pypandoc installed
with open(os.path.join(ROOT, 'README.md'), encoding="utf-8") as f:
    README = f.read()

setup(name='ukfs',
      version=__version__,
      description='Python kernel feature selection',
      long_description=README,
      long_description_content_type='text/markdown', 
      author=u'Jerome Mariette',
      author_email='jerome.mariette@inrae.fr',
      url='https://forgemia.inra.fr/jerome.mariette/ukfs',
      packages=find_packages(),
      platforms=['linux','macosx','windows'],
      license = 'GPL',
      scripts=[],
      data_files=[],
      requires=["numpy","scipy",'autograd'],
      install_requires=["numpy","scipy",'autograd'],
      classifiers=[
        'Development Status :: 4 - Beta',
        'Intended Audience :: Developers',
        'Environment :: Console',
        'Operating System :: OS Independent',
        'Operating System :: MacOS',
        'Operating System :: POSIX',
        'Programming Language :: Python',
        'Topic :: Utilities'
    ]
)