# ukfs Unsupervised Kernel Feature Selection

A feature selection algorithm that is adapted to the kernel framework that does not rely on any particular assumption on the
data structure but can incorporate these assumptions in a very flexible way, if needed. The method is expressed under the form
of a non-convex optimization problem with a L1 penalty, which is solved with a proximal gradient descent approach.

## Installation

The following Python modules are necessary and intalled automaticaly with:

- Numpy (>=1.11)
- Scipy (>=1.0)
- autograd

You can install the package by downloading it and then running:
```
python setup.py install --user # for user install (no root)
```

## Citation

Mariette J., Brouard C. Flamary R. and Vialaneix N. (2020). Unsupervised variable selection for kernel methods in systems
biology. Preprint.